import 'package:demo_flutter_mvvm/model/images.dart';
import 'package:demo_flutter_mvvm/view_model/home_view_model.dart';
import 'package:flutter/material.dart';

import 'package:demo_flutter_mvvm/utils/utility.dart';

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

class HomeState extends State<Home>{

  HomeViewModel _viewModel = HomeViewModel();

  final GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();

  List<Images> images;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _viewModel.fetchImages(_scaffoldState, context).then((data){
      if(data != null){
        images = data;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldState,
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  Widget _buildAppBar(){
    return AppBar(
      title: Text("Trang chính"),
      centerTitle: true,
    );
  }

  Widget _buildBody(){
    return StreamBuilder(
      stream: _viewModel.outputIsLoading,
      builder: (context, snapshot){
        return snapshot.data == null?_buildProgressDialog():(snapshot.data?_buildProgressDialog():_buildGridView());
      },
    );
  }

  Widget _buildProgressDialog(){
    return Opacity(
      opacity: 0.5,
      child: Container(
        color: Colors.grey,
        height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildGridView(){
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: _buildGridTile(images),
      ),
    );
  }

  List<Widget> _buildGridTile(List<Images> images){
    List<Container> containers = List<Container>.generate(images.length, (index){
      return Container(
        padding: EdgeInsets.all(5.0),
        child: FadeInImage.assetNetwork(
        image: BASE_URL + images[index].url,
        placeholder: 'assets/loading.gif',
        fit: BoxFit.fill,
        )
      );
    });
    return containers;
  }
}