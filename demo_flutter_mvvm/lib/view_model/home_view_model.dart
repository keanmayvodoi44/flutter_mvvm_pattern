import 'dart:async';

import 'package:demo_flutter_mvvm/model/images.dart';
import 'package:flutter/material.dart';
import 'package:demo_flutter_mvvm/utils/utility.dart';

abstract class HomeContract{
  Stream<bool> get outputIsLoading;
  Stream<List<Images>> get outputLoadImages;

  void dispose();
}

class HomeViewModel extends HomeContract{

  Images _model = Images();

  StreamController<bool> _isLogin = StreamController.broadcast();
  StreamController<List<Images>> _listImages = StreamController.broadcast();

  @override
  // TODO: implement outputIsLoading
  Stream<bool> get outputIsLoading => _isLogin.stream;

  @override
  // TODO: implement outputLoadImages
  Stream<List<Images>> get outputLoadImages => _listImages.stream;

  Future<List<Images>> fetchImages(GlobalKey<ScaffoldState> scaffoldState, BuildContext context) async {
    _isLogin.add(true);
    List<Images> images;
    await _model.fetch().then((data){
      _isLogin.add(false);
      images = data;
    }).catchError((onError){
      _isLogin.add(false);
      showSnackbar(scaffoldState, "Không thể kết nối đến Server");
    });
    return images;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _isLogin.close();
    _listImages.close();
  }

}