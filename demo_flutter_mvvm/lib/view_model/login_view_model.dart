import 'dart:async';

import 'package:demo_flutter_mvvm/model/users.dart';
import 'package:demo_flutter_mvvm/view/home.dart';
import 'package:flutter/material.dart';
import 'package:demo_flutter_mvvm/utils/utility.dart';

abstract class LoginContract{
  Sink get inputAccountText;
  Sink get inputPasswordText;
  Stream<bool> get outputIsEnableButton;
  Stream<bool> get outputIsLogin;

  void dispose();
}

class LoginViewModel extends LoginContract{

  Users _model = Users();

  StreamController<String> _accountTextController = StreamController.broadcast();
  StreamController<String> _passwordTextController = StreamController.broadcast();
  StreamController<bool> _isEnableButtonController = StreamController.broadcast();
  StreamController<bool> _isLogin = StreamController.broadcast();

  bool _accountIsEmpty = true;
  bool _passwordIsEmpty = true;

  LoginViewModel(){
    this._isEnableButtonController.add(false);
    this._accountTextController.stream.listen((data){
      if(data != ""){
        _accountIsEmpty = false;
      }
      else{
        _accountIsEmpty = true;
      }
      checkEmpty();
    });
    
    this._passwordTextController.stream.listen((data){
      if(data != ""){
        _passwordIsEmpty = false;
      }
      else{
        _passwordIsEmpty = true;
      }
      checkEmpty();
    });
    this._isLogin.add(false);
  }

  void checkEmpty(){
    if(_accountIsEmpty == false && _passwordIsEmpty == false){
      _isEnableButtonController.add(true);
    }
    else{
      _isEnableButtonController.add(false);
    }
  }

  void login (GlobalKey<ScaffoldState> scaffoldState, BuildContext context, String account, String password){
    _isLogin.add(true);
    _model.login(account, password).then((data){
      _isLogin.add(false);
      if(data == 1){
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => Home()
            )
        );
      }
      else{
        showSnackbar(scaffoldState, "Tài khoản không hợp lệ");
      }
    }).catchError((onError){
      _isLogin.add(false);
      showSnackbar(scaffoldState, "Không thể kết nối đến Server");
    });
  }

  @override
  // TODO: implement inputAccountText
  Sink get inputAccountText => _accountTextController;

  @override
  // TODO: implement inputPasswordText
  Sink get inputPasswordText => _passwordTextController;

  @override
  // TODO: implement outputIsEnableButton
  Stream<bool> get outputIsEnableButton => _isEnableButtonController.stream;

  @override
  void dispose() {
    // TODO: implement dispose
    _accountTextController.close();
    _passwordTextController.close();
    _isEnableButtonController.close();
    _isLogin.close();
  }

  @override
  // TODO: implement outputIsLogin
  Stream<bool> get outputIsLogin => _isLogin.stream;
}